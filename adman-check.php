<?php
error_reporting(E_ALL);
ini_set("memory_limit", "256M");
set_time_limit(1800);

class AdmanCheck extends ViewAdman
{
    private $request;

    protected $extensionsCheck = array('curl', 'mysqli', 'date', 'zip', 'hash', 'json', 'mbstring', 'pcre', 'simplexml', 'openssl', 'IonCube Loader');

    protected $minMemoryLimit = 256;

    protected $checkUpTimeLogFile = 60;

    protected $minPHPVersion = '5.6.0';

    protected $rootDir = __DIR__;

    protected $cronLogsDir = "/var/cron_logs";

    protected $checkLogFiles = array('core_cron', 'test_db_response', 'test_db_response_time');

    protected $excludeDirs = array('.git', '.idea');

    protected $urlRequestIntegrityCheck;

    protected $urlRequestWebsitesIntegrityCheck;

    protected $typeCheck = "check";

    protected $pathPHPBin;

    protected $publicUrl;

    function __construct($request)
    {
        $this->request = $request;
        $this->pathPHPBin = $this->getPHPExecutableFromPath();
        $this->publicUrl = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
        $this->typeCheck = (isset($request['check_all'])) ? 'check_all' : $this->typeCheck;
        $this->urlRequestIntegrityCheck = $this->publicUrl . "/adman-check.php?demoServer-IntegrityCheck";
        $this->urlRequestWebsitesIntegrityCheck = $this->publicUrl . "/adman-check.php?demoServer-WebsitesIntegrityCheck";
    }

    private function checkIntegrity()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->urlRequestIntegrityCheck);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $content = curl_exec($curl);
        curl_close($curl);
        return is_array($res = json_decode($content, true)) ? $res : false;
    }

    private function checkWebsitesIntegrity()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->urlRequestWebsitesIntegrityCheck);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $content = curl_exec($curl);
        curl_close($curl);
        return is_array($res = json_decode($content, true)) ? $res : false;
    }


    private function checkFailsLogs()
    {
        $res = $error = array();
        try {
            $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->rootDir . $this->cronLogsDir));
            $it->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
            while ($it->valid()) {
                if ($it->isFile())
                    $res[basename($it->getPathname())] = array(
                        'path' => realpath($it->getPathname()),
                        'is_writable' => is_writable($it->getPathname()),
                        'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                        'name' => basename($it->getPathname()),
                        'filemtime' => filemtime($it->getPathname())
                    );
                $it->next();
            }
        } catch (Exception $e) {
            $error[] = $e;
        }
        $result = array();
        foreach ($this->checkLogFiles as $item) {
            $result[] = array(
                'path' => isset($res[$item]['path']) ? $res[$item]['path'] : null,
                'name' => $item,
                'is_exist' => (bool)in_array($item, array_keys($res)),
                'uptime' => isset($res[$item]['filemtime']) ? ((bool)in_array($item, array_keys($res)) ? $res[$item]['filemtime'] : false) : false,
                'check_time' => isset($res[$item]['filemtime']) ? ((time() - $res[$item]['filemtime']) <= $this->checkUpTimeLogFile ? true : false) : false
            );
        }

        return $result;
    }

    private function getPHPExecutableFromPath()
    {
        $paths = explode(PATH_SEPARATOR, getenv('PATH'));
        foreach ($paths as $path) {
            if (strstr($path, 'php.exe') && isset($_SERVER["WINDIR"]) && file_exists($path) && is_file($path)) {
                return $path;
            } else {
                $php_executable = $path . DIRECTORY_SEPARATOR . "php" . (isset($_SERVER["WINDIR"]) ? ".exe" : "");
                if (file_exists($php_executable) && is_file($php_executable)) {
                    return $php_executable;
                }
            }
        }

        if (getenv('PHPBIN'))
            return getenv('PHPBIN');

        if (PHP_BINARY)
            return PHP_BINARY;

        return false;
    }

    private function dirList($type = 'all')
    {
        $res = $error = array();
        try {
            $directory = new RecursiveDirectoryIterator($this->rootDir);
            $it = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
            while ($it->valid()) {
                if (!array_intersect($this->excludeDirs, explode(DIRECTORY_SEPARATOR, $it->getPathname()))) {
                    if ($type == 'all') {
                        $res[] = array(
                            'path' => realpath($it->getPathname()),
                            'is_writable' => is_writable($it->getPathname()),
                            'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                            'is_dir' => $it->isDir(),
                        );
                    }
                    if ($type == 'fail') {
                        if (!is_writable($it->getPathname()))
                            $res[] = array(
                                'path' => realpath($it->getPathname()),
                                'is_writable' => is_writable($it->getPathname()),
                                'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                                'is_dir' => $it->isDir()
                            );
                    }
                }
                $it->next();
            }
        } catch (Exception $e) {
            $error = $e;
        }
        return array_unique($res, SORT_REGULAR);
    }

    private function checkTimeOut()
    {
        if ($terminateTimeout = ini_get('request_terminate_timeout')) {
            if ($terminateTimeout >= 1800)
                return true;
        } elseif ($executionTimeout = ini_get('max_execution_time')) {
            if ($executionTimeout >= 1800)
                return true;
        }
        return false;
    }

    private function check_http_status($url)
    {
        $user_agent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        $err = curl_error($ch);
        if (!empty($err))
            return $err;
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpcode;
    }

    private function checkPublicUrl($type = '')
    {
        if ($type == 'rand')
            return $this->check_http_status($this->publicUrl . '/r/test' . rand(1, 99)) == 200 ? true : false;

        return $this->check_http_status($this->publicUrl . '/r/url.php') == 200 ? true : false;
    }

    private function additionalCheck()
    {
        $result = array();
        $result[] = array('check' => 'timeout', 'isPass' => $this->checkTimeOut());
        $result[] = array('check' => 'publicAccessUrl', 'isPass' => $this->checkPublicUrl());
        $result[] = array('check' => 'checkRedirected', 'isPass' => $this->checkPublicUrl('rand'));
        return $result;

    }

    private function configurationCheck($isPass = false)
    {
        $result = array();
        $result[] = array('check' => 'mysql', 'isPass' => (bool)function_exists('mysqli_connect'));
        $result[] = array('check' => 'phpversion', 'isPass' => (bool)version_compare(phpversion(), $this->minPHPVersion, '>='));
        $result[] = array('check' => 'safe_mode', 'isPass' => !ini_get('safe_mode') ? true : false);
        $result[] = array('check' => 'short_open_tag', 'isPass' => ini_get('short_open_tag') ? true : false);
        $result[] = array('check' => 'memory_limit', 'isPass' => (int)ini_get('memory_limit') >= $this->minMemoryLimit ? true : false);
        $result[] = array('check' => 'exec', 'isPass' => (bool)function_exists('exec'));
        $result[] = array('check' => 'shell_exec', 'isPass' => (bool)function_exists('shell_exec'));
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'Gzip', 'isPass' => shell_exec('gzip -V') ? true : false);
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'nice', 'isPass' => shell_exec('nice --version') ? true : false);
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'mysqldump', 'isPass' => shell_exec('mysqldump --version') ? true : false);
        $result[] = array('check' => 'popen', 'isPass' => function_exists('popen') ? true : false);
        foreach ($this->extensionsCheck as $extension) {
            $result[] = array('check' => $extension, 'isPass' => (bool)extension_loaded($extension) ? true : false);
        }
        if ($isPass) {
            $isPass = $result;
            $result = array();
            foreach ($isPass as $item)
                if (!$item['isPass'])
                    $result[] = $item;
        }
        return $result;
    }

    public function init()
    {
        switch ($this->typeCheck) {
            case 'check_all':
                $checkLogFiles = $this->viewFailLogFails($this->checkFailsLogs());
                $configuration = $this->viewFailCheck(
                    $this->configurationCheck(),
                    $this->dirList('fail'), $this->typeCheck);
                $integrity = $this->viewFailCheckIntegrity($this->checkIntegrity());
                $websitesIntegrity = $this->viewFailCheckWebsitesIntegrity($this->checkWebsitesIntegrity());
                $content = $configuration . $this->viewFailAdditional($this->additionalCheck()) . $integrity . $checkLogFiles . $websitesIntegrity;
                break;
            default:
                $conf = (count($this->configurationCheck(true))) ? $this->configurationCheck(true) : array();
                $dirList = (count($conf)) ? array() : $this->dirList('fail');
                $configuration = $this->viewFailCheck($conf, $dirList);
                $checkLogFiles = $this->viewFailLogFails((count($conf)) ? array() : $this->checkFailsLogs());
                $integrity = $this->viewFailCheckIntegrity((count($conf)) ? array() : $this->checkIntegrity());
                $content = $configuration . $this->viewFailAdditional((count($conf)) ? array() : $this->additionalCheck()) . $integrity . $checkLogFiles;
        }

        echo $this->body($content);

    }

}

if (isset($_GET['demoServer-IntegrityCheck'])) {
    $errors = array();
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 1'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 2'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 3'
    );
    $errors[] = array(
        'error' => false,
        'message' => 'Error message IntegrityCheck 4'
    );
    echo json_encode($errors);
} elseif(isset($_GET['demoServer-WebsitesIntegrityCheck'])) {
    $errors = array();
    $errors[] = array(
        'error' => true,
        'message' => 'Error message WebsitesIntegrityCheck 1'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message WebsitesIntegrityCheck 2'
    );
    $errors[] = array(
        'error' => false,
        'message' => 'Error message WebsitesIntegrityCheck 3'
    );
    echo json_encode($errors);
} else {
    $check = new AdmanCheck($_GET);
    $check->init();
}


class ViewAdman
{

    private $errorDescriptionArray = array(
        'curl' => 'PHP extension {ext} must be installed',
        'mysqli' => 'PHP extension {ext} must be installed',
        'date' => 'PHP extension {ext} must be installed',
        'zip' => 'PHP extension {ext} must be installed',
        'hash' => 'PHP extension {ext} must be installed',
        'json' => 'PHP extension {ext} must be installed',
        'mbstring' => 'PHP extension {ext} must be installed',
        'pcre' => 'PHP extension {ext} must be installed',
        'simplexml' => 'PHP extension {ext} must be installed',
        'openssl' => 'PHP extension {ext} must be installed',
        'IonCube Loader' => 'PHP extension {ext} must be installed',
        'mysql' => 'Mysql function is <strong>disabled</strong>',
        'phpversion' => 'You need<strong> PHP 5.6.0</strong> (or greater)',
        'safe_mode' => 'PHP safe_mode is <strong>on</strong>',
        'short_open_tag' => 'PHP short_open_tag is <strong>off</strong>',
        'memory_limit' => 'PHP memory_limit must be at least <strong>256M</strong>',
        'exec' => 'Exec function is <strong>disabled</strong>',
        'shell_exec' => 'Shell_exec function is <strong>disabled</strong>',
        'Gzip' => 'Gzip command is <strong>disabled</strong>',
        'nice' => 'Nice command is <strong>disabled</strong>',
        'popen' => 'Popen function is <strong>disabled</strong>',
        'mysqldump' => 'Mysqldump command is <strong>disabled</strong>',
        'checkFileAndDirAdMan' => 'Error no permission to write the file',
        'timeout' => 'PHP timeout must be at least <strong>1800</strong>',
        'publicAccessUrl' => 'You need to open the public access to script /r/url.php',
        'checkRedirected' => 'All requests to folder /r/ must be redirected to /r/url.php this setting must be relevant to this htaccess rule: RewriteRule .* url.php?param=$0 [L]'
    );

    protected function errorDescription($ext = "")
    {
        return $this->errorDescriptionArray[$ext] ?
            str_replace("{ext}",
                "<a target='_blank' href='https://www.google.ru/search?q={$ext}+install+php'>{$ext}</a>",
                $this->errorDescriptionArray[$ext])
            : "Error {$ext} ";
    }

    protected function viewFailLogFails($data = array())
    {
        $html = "";
        $ds = DIRECTORY_SEPARATOR;
        $helperArr = array(
            'core_cron' => "{$ds}cron.php",
            'test_db_response' => "{$ds}api{$ds}test_db_response.php loop=1",
            'test_db_response_time' => "{$ds}api{$ds}test_db_response_time.php loop=1"
        );
        $defaultPhpPath = "&lt;PATH_TO_PHP&gt;";
        foreach ($data as $key => $item) {
            if (!$item['is_exist'] || !$item['check_time']) {
                $desc = '<p>* * * * * ' . ($this->pathPHPBin ? $this->pathPHPBin : $defaultPhpPath) . '  ' . $this->rootDir . '' . (isset($helperArr[$item['name']]) ? $helperArr[$item['name']] : "") . '</p>';
                $html .= '<li><div><b>' . $item['name'] . ':</b>' . $desc . '</div><span>' . ((!$item['is_exist']) ? "no file" : "file is out of date") . '</span></li>';
            }
        }
        return $html ? '<div class="name-section">Cron Check</div><ul class="fail-list-file">' . $html . "</ul>" : false;
    }

    protected function viewFailCheckWebsitesIntegrity($data)
    {
        if (gettype($data) != "boolean") {
            $html = "";
            foreach ($data as $item) {
                if (isset($item['error']) && isset($item['message'])) {
                    if ($item['error'])
                        $html .= '<li><div>' . $item['message'] . '</div></li>';
                }
            }
            return $html ? '<div class="name-section">Websites Integrity Check</div><ul class="fail-list">' . $html . '</ul>' : false;
        } else return "<div class=\"name-section\">Websites Integrity Check</div><li><div>Verification server is not available</div></li>";
    }

    protected function viewFailCheckIntegrity($data)
    {
        if (gettype($data) != "boolean") {
            $html = "";
            foreach ($data as $item) {
                if (isset($item['error']) && isset($item['message'])) {
                    if ($item['error'])
                        $html .= '<li><div>' . $item['message'] . '</div></li>';
                }
            }
            return $html ? '<div class="name-section">Integrity Check</div><ul class="fail-list">' . $html . '</ul>' : false;
        } else return "<div class=\"name-section\">Integrity Check</div><li><div>Verification server is not available</div></li>";
    }

    protected function viewFailAdditional($data = array())
    {
        $html = "";
        foreach ($data as $item) {
            if (!$item['isPass']) {
                $html .= '<li><div>' . $this->errorDescription($item['check']) . '</div></li>';
            }
        }
        return $html ? '<div class="name-section">Additional Server Requirements</div><ul class="fail-list">' . $html . '</ul>' : false;
    }

    protected function viewFailCheck($data = array(), $dataFile = array())
    {
        $html = $checkFile = "";
        foreach ($data as $item) {
            if (!$item['isPass']) {
                $html .= '<li><div>' . $this->errorDescription($item['check']) . '</div></li>';
            }
        }
        foreach ($dataFile as $file) {
            $checkFile .= '<li class="fail-list-file"><div title="' . $file['path'] . '">' . $file['path'] . '</div><span>' . $file['fileperms'] . '</span></li>';
        }

        $checkFile = $checkFile ? "<li><div>Error no permission to write the file:</div></li>" . $checkFile : false;
        $html = $html ? "<ul class='fail-list'>" . $html . $checkFile . "</ul>" : false;
        return $html ? '<div class="name-section">Сritical Server Requirements</div>' . $html : false;
    }


    public function body($content = "")
    {
        if (!$content)
            $content = '<p style="color:#0c850c;"><strong>Congratulations!</strong><br>Your server meets the requirements for TM Ad Manager.</p>';
        return <<<HTML
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0">
    <title>AdmanCheck</title>
    <link rel="shortcut icon" href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAuLi4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAREAAAAAAAARERAAAAAREBEREQAAARAQAREREAARARAAERERARARAAABERARARAAAAARERARAAAAAAABEBAAAAAAAQARAAAAABERAAEQAAABERERABEAABERERAAAREAEAEREAABERAAABEQAAEREAAAEQAAABERAAEQAAAAABCP/wAAB/EAAAPlAACByQAAwJMAAOEnAADwTwAA/l8AAPs/AADDnwAAgM8AAAHjAABh4QAA8eEAAPPwAADn/QAA" />
<style>
 *
{
	margin:0;
	padding:0
}

li
{
	list-style-type:none;
}

li:last-child
{
    margin-bottom: 0;
}


ul
{
	margin-left:0;
	padding-left:0
}

body
{
	margin:0;
	font-family:Roboto,sans-serif;
	font-size:.9rem;
	font-weight:400;
	line-height:1.6;
	color:#212529;
	text-align:left;
	background-color:#f4f3ef
}

.content
{
	max-width:1200px;
	margin:2% auto;
	padding:15px;
	background:#fff;
	box-shadow:0 6px 10px -4px rgba(0,0,0,.15)!important
}

.fail-title
{
	display:block;
	color:#e36209
}

.fail-list-file span,.fail-list-file div
{
	display:inline-block
}

.fail-list-file div
{
	width:80%;
	color:#949393;
	font-weight:100;
	text-overflow:ellipsis
}

.fail-list-file span
{
	font-weight:700;
	text-align:right;
	width:20%;
	color:#e36209
}

a
{
	color:#0089ff;
	border-bottom:2px dotted rgba(0,137,255,.3)!important;
	text-decoration-line:none
}

a:hover
{
	color:#0e36ff
}

.name-section
{
	font-size:14pt;
	font-weight:700;
	margin-top:15px;
	line-height: initial;
}

.name-section:first-child{
    margin: 0;
}
</style>
</head>
<body>
<div class="content">
{$content}
</div>
</body>
</html>

HTML;
    }

}
